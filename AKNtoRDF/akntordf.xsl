<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet 
	version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
	xmlns:eli="http://data.europa.eu/eli#"
	xmlns:dc="http://purl.org/dc/elements/1.1/"
	xpath-default-namespace="http://docs.oasis-open.org/legaldocml/ns/akn/3.0/CSD13"
	exclude-result-prefixes="dc"
	>
	
	<xsl:output indent="yes" version="1.0" method="xml" omit-xml-declaration="no" />
	
	
	<xsl:param name="uri1" select= "substring-after(descendant::meta/identification/FRBRWork/FRBRuri/@value,'/gr/')" /><!-- pd/2015/1/, the law-->
	<xsl:param name="typelaw" select= "substring-before($uri1,'/')" /> <!-- pd/act, the law type-->
	<xsl:param name="fek1a" select= "substring-after($uri1,'/')" /> <!-- 2015/1/ OR, in egk case, our local id, example: 2015/942f328e-272e-49b0-8507-3f35af57c25f/-->
	<xsl:param name="fek1" select= "substring-after($fek1a,'/')" />  <!-- 1/, law number-->
	<xsl:param name="fek2a" select= "substring-after(akomaNtoso/act/meta/publication/@number, 'ΤΕΥΧΟΣ ')" />  <!--ΠΡΩΤΟ    Αρ.-->
	<xsl:param name="fek2" select= "substring-before($fek2a, ' ')" />  <!--ΠΡΩΤΟ-->
	<xsl:param name="fek3" select= "concat('/', akomaNtoso/act/meta/publication/@date)" />  <!--date-->
	<xsl:param name="rooturi" select="concat('http://legislation.ntua.gr/eli/', $uri1)"/>  <!--law's uri-->
	<xsl:param name="localid" select="concat($fek1, $fek2, $fek3)"/>  <!--fek-->
	<xsl:param name="language" select="descendant::meta/identification/FRBRExpression/FRBRlanguage/@language"/> <!--language-->

	
	<xsl:template match="/">
		<rdf:RDF 
			xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
			xmlns:eli="http://data.europa.eu/eli#"> 
			<xsl:apply-templates select="akomaNtoso/act" />
			<xsl:apply-templates select="akomaNtoso/doc" />
		</rdf:RDF>
	</xsl:template>

	<xsl:template match="akomaNtoso/act"> <!--law's metadata, then cover, intro, chapters/articles/paragraphs and outro descriptions and content-->
		<!--metadata-->
		<eli:LegalResource rdf:about="{$rooturi}"> 
			<eli:first_date_entry_in_force rdf:datatype="http://www.w3.org/2001/XMLSchema#date"> <xsl:value-of select="meta/identification/FRBRWork/FRBRdate/@date"/> </eli:first_date_entry_in_force>
			<eli:date_publication rdf:datatype="http://www.w3.org/2001/XMLSchema#date"><xsl:value-of select="meta/publication/@date"/></eli:date_publication>
			<eli:id_local><xsl:value-of select ="$localid"/></eli:id_local>
			<eli:passed_by rdf:resource="http://legislation.ntua.gr/eli#Πρόεδρος_Δημοκρατίας"/>
			<eli:passed_by rdf:resource="http://legislation.ntua.gr/eli#Υπουργός"/>
			<eli:is_published_by rdf:resource="http://legislation.ntua.gr/eli#Εθνικό_Τυπογραφείο"/>
			<xsl:apply-templates select="meta/proprietary" /> <!--reach DCTerms for title-->
			<xsl:if test="$typelaw='pd'">	
				<eli:ResourceType rdf:resource="http://legislation.ntua.gr/eli#Presidential_Decree"/>
			</xsl:if>
			<xsl:if test="$typelaw='act'">
				<eli:ResourceType rdf:resource="http://legislation.ntua.gr/eli#Legislative_Act"/>
			</xsl:if>
			<xsl:apply-templates select="//a" /> <!--search for citations-->
			<eli:has_part rdf:resource="{concat($rooturi,'cover')}"/>
			<eli:has_part rdf:resource="{concat($rooturi,'intro')}"/>
			<xsl:apply-templates select="coverPage"/> <!--parts of the law-->
			<eli:has_part rdf:resource="{concat($rooturi,'conc')}"/>
		</eli:LegalResource>	
		
		<!--cover-->		
		<eli:LegalResource rdf:about="{concat($rooturi,'cover/')}">
		<eli:LegalResourceSubdivision rdf:resource="http://legislation.ntua.gr/eli#Cover"/>
		<text xmlns="http://legislation.ntua.gr/eli#" xml:lang="{$language}"><xsl:value-of select="coverPage/p" /> </text>
		</eli:LegalResource>
		
		<!--intro-->
		<eli:LegalResource rdf:about="{concat($rooturi,'intro/')}">
		<eli:LegalResourceSubdivision rdf:resource="http://legislation.ntua.gr/eli#Introduction"/>
		<text xmlns="http://legislation.ntua.gr/eli#" xml:lang="{$language}"><xsl:value-of select="preamble/p" /> </text>
		</eli:LegalResource>
		
		<xsl:apply-templates  select="body"/> <!--descriptions and text for chapters/articles/paragraphs-->
		
		<!--outro-->
		<eli:LegalResource rdf:about="{concat($rooturi,'conc/')}">
		<eli:LegalResourceSubdivision rdf:resource="http://legislation.ntua.gr/eli#Conclusion"/>
		<text xmlns="http://legislation.ntua.gr/eli#" xml:lang="{$language}"><xsl:value-of select="conclusions/p" /> </text>
		</eli:LegalResource>
		
	</xsl:template>		
	
	<xsl:template match="akomaNtoso/doc"> <!--egk's metadata, then cover, intro, chapters/articles/paragraphs and outro descriptions and content-->
		<!--metadata-->
		<eli:LegalResource rdf:about="{$rooturi}"> 
			<eli:first_date_entry_in_force rdf:datatype="http://www.w3.org/2001/XMLSchema#date"> <xsl:value-of select="meta/identification/FRBRWork/FRBRdate/@date"/> </eli:first_date_entry_in_force>
			<eli:date_publication rdf:datatype="http://www.w3.org/2001/XMLSchema#date"><xsl:value-of select="meta/publication/@date"/></eli:date_publication>
			<eli:id_local><xsl:value-of select ="$fek1a"/></eli:id_local>
			<eli:passed_by rdf:resource="http://legislation.ntua.gr/eli#Πρόεδρος_Δημοκρατίας"/>
			<eli:passed_by rdf:resource="http://legislation.ntua.gr/eli#Υπουργός"/>
			<eli:is_published_by rdf:resource="http://legislation.ntua.gr/eli#Εθνικό_Τυπογραφείο"/>
			<xsl:apply-templates select="meta/proprietary" /> <!--reach DCTerms for title-->
			<eli:ResourceType rdf:resource="http://legislation.ntua.gr/eli#Εγκύκλιος"/>
			<xsl:apply-templates select="//a" /> <!--search for citations-->
			<eli:has_part rdf:resource="{concat($rooturi,'cover')}"/>
			<eli:has_part rdf:resource="{concat($rooturi,'intro')}"/>
			<xsl:apply-templates select="coverPage"/> <!--parts of the law-->
			<eli:has_part rdf:resource="{concat($rooturi,'conc')}"/>
		</eli:LegalResource>	
		
		<!--cover-->		
		<eli:LegalResource rdf:about="{concat($rooturi,'cover/')}">
		<eli:LegalResourceSubdivision rdf:resource="http://legislation.ntua.gr/eli#Cover"/>
		<text xmlns="http://legislation.ntua.gr/eli#" xml:lang="{$language}"><xsl:value-of select="coverPage/p" /> </text>
		</eli:LegalResource>
		
		<!--intro-->
		<eli:LegalResource rdf:about="{concat($rooturi,'intro/')}">
		<eli:LegalResourceSubdivision rdf:resource="http://legislation.ntua.gr/eli#Introduction"/>
		<text xmlns="http://legislation.ntua.gr/eli#" xml:lang="{$language}"><xsl:value-of select="preamble/p" /> </text>
		</eli:LegalResource>
		
		<xsl:apply-templates  select="body"/> <!--descriptions and text for chapters/articles/paragraphs-->
		
		<!--outro-->
		<eli:LegalResource rdf:about="{concat($rooturi,'conc/')}">
		<eli:LegalResourceSubdivision rdf:resource="http://legislation.ntua.gr/eli#Conclusion"/>
		<text xmlns="http://legislation.ntua.gr/eli#" xml:lang="{$language}"><xsl:value-of select="conclusions/p" /> </text>
		</eli:LegalResource>
		
	</xsl:template>		
			
	<!--in order to reach DCTerms, we have to do this-->
	<xsl:template match="meta/proprietary">
		<xsl:apply-templates select="child::*" />
	</xsl:template>	

	<xsl:template match="child::*">
	<eli:title xml:lang="{$language}"><xsl:value-of select="dc:title"/></eli:title>
	</xsl:template>	
	
	<xsl:template match="coverPage"> <!--parts of law-->
		<xsl:for-each select="toc/tocItem">
			<xsl:if test="@level=1">
				<xsl:variable name="testuri" select="substring-after(@href, '/main/')" />  <!-- art/1/ -->
				<xsl:variable name="typeuri" select= "substring-before($testuri,'/')" /> <!-- art or chp, the type of law part -->
				<xsl:if test="$typeuri='chp'">
					<xsl:variable name="numburi" select= "substring-after($testuri,'/')" />	<!-- 1/ -->
					<xsl:variable name="newuri" select="concat($rooturi, 'chapter/', $numburi)"/> 
					<eli:has_part rdf:resource="{$newuri}"/>
				</xsl:if>
				<xsl:if test="$typeuri='art'">
					<xsl:variable name="numburi" select= "substring-after($testuri,'/')" /> 	<!-- 1/ -->
					<xsl:variable name="newuri" select="concat($rooturi, 'article/', $numburi)"/> 
					<eli:has_part rdf:resource="{$newuri}"/>
				</xsl:if>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	
	<xsl:template match="//a"> <!--citations-->
		<xsl:variable name="law1" select= "substring-after(@href,'gr/')" />  <!--act/2014/4275/art/5/para/1-->
		<xsl:variable name="lawtype" select= "substring-before($law1,'/')" />  <!--act/pd, the law type-->
		<xsl:variable name="law2" select= "substring-after($law1,'/')" />  <!--2014/4275/art/5/para/1-->
		<xsl:variable name="lawdate" select= "substring-before($law2,'/')" />  <!--2014, the law date-->
		<xsl:variable name="law3" select= "substring-after($law2,'/')" />  <!--4275/art/5/para/1-->
		<xsl:variable name="lawnum" select= "substring-before($law3,'/')" />  <!--4275, the law number-->
		<xsl:variable name="lawrefer" select= "concat('http://legislation.ntua.gr/eli/', $lawtype, '/', $lawdate, '/', $lawnum)" />  <!--the referenced law-->
		<eli:cites rdf:resource="{$lawrefer}"/>
	</xsl:template>
	
	<xsl:template match="body"> 
		<!--case of articles only-->
			<xsl:for-each select="../body/article"> <!--descriptions for articles that have no chapter, aka they are directly under ''body''-->
				<xsl:variable name="artinum" select= "substring-after(@eId,'/')" />  <!--1/, article number-->
				<xsl:variable name="artiuri" select= "concat($rooturi, 'article/', $artinum)" />  <!-- article's uri-->
				<eli:LegalResource rdf:about="{$artiuri}">
				<eli:LegalResourceSubdivision rdf:resource="http://legislation.ntua.gr/eli#Article"/>
				<eli:number><xsl:value-of select="num" /></eli:number>
				<eli:subdivision_title xmlns="http://legislation.ntua.gr/eli#"> <xsl:value-of select="heading" /> </eli:subdivision_title>
				<xsl:for-each select="paragraph"> <!--uri for each paragraph-->
					<xsl:variable name="paranum" select= "substring-after(@eId,'para/')" />  <!--1/, paragraph number-->
					<eli:has_part rdf:resource="{concat($artiuri, 'paragraph/', $paranum)}"/>
				</xsl:for-each>
				</eli:LegalResource>
			</xsl:for-each>
			
			<xsl:for-each select="../body/article"> <!--text of each article-->
				<xsl:if test="./content/p"> <!--if we have no content for the article itself, we don't need to make a new node for this-->
					<xsl:variable name="artinum1" select= "substring-after(@eId,'/')" /> <!--1/, article number-->
					<xsl:variable name="artiuri1" select= "concat($rooturi, 'article/', $artinum1)" />  <!-- article's uri-->
					<eli:LegalResource rdf:about="{$artiuri1}">
					<eli:LegalResourceSubdivision rdf:resource="http://legislation.ntua.gr/eli#Article"/>
					<eli:subdivision_title xmlns="http://legislation.ntua.gr/eli#"> <xsl:value-of select="heading" /> </eli:subdivision_title>
					<text xmlns="http://legislation.ntua.gr/eli#" xml:lang="{$language}"> <xsl:value-of select="content/p"/></text>
					</eli:LegalResource>
				</xsl:if>
			</xsl:for-each>
			
			<xsl:for-each select="../body/article/paragraph"> <!--text of each paragraph-->
				<xsl:variable name="artinum2" select= "substring-after(../@eId,'art/')" />  <!--1/, article number-->
				<xsl:variable name="artiuri2" select= "concat($rooturi, 'article/', $artinum2)" />  <!-- article's uri-->
				<xsl:variable name="artiparanum" select= "substring-after(@eId,'para/')" /> <!--1/, paragraph number-->
				<xsl:variable name="artiparauri" select= "concat($artiuri2, 'paragraph/', $artiparanum)" />  <!-- paragraph's uri-->
				<eli:LegalResource rdf:about="{$artiparauri}">
				<eli:LegalResourceSubdivision rdf:resource="http://legislation.ntua.gr/eli#Paragraph"/>
				<eli:number><xsl:value-of select="num" /></eli:number>
				<text xmlns="http://legislation.ntua.gr/eli#" xml:lang="{$language}"> <xsl:value-of select="content/p"/></text>
				</eli:LegalResource>
			</xsl:for-each>
	
	<!--case of both articles and chapters-->
			<xsl:for-each select="chapter"> <!--descriptions for chapters-->
				<xsl:variable name="chapnum" select= "substring-after(@eId,'/')" /> <!--1/, chapter number-->
				<xsl:variable name="chapuri" select= "concat($rooturi, 'chapter/', $chapnum)" />  <!-- chapter's uri-->
				<eli:LegalResource rdf:about="{$chapuri}">
				<eli:LegalResourceSubdivision rdf:resource="http://legislation.ntua.gr/eli#Chapter"/>
				<eli:number><xsl:value-of select="num" /></eli:number>
				<eli:subdivision_title xmlns="http://legislation.ntua.gr/eli#"> <xsl:value-of select="heading" /> </eli:subdivision_title>
				<xsl:for-each select="article"> <!--uri for each article-->
					<xsl:variable name="artnum" select= "substring-after(@eId,'art/')" /> <!--1/, article number-->
					<eli:has_part rdf:resource="{concat($chapuri, 'article/', $artnum)}"/>
				</xsl:for-each>
				</eli:LegalResource>
			</xsl:for-each>
			
			<xsl:for-each select="chapter/article"> <!--descriptions for articles under chapters-->
				<xsl:variable name="chapnum1" select= "substring-after(../@eId,'/')" />  <!--1/, chapter number-->
				<xsl:variable name="chapuri1" select= "concat($rooturi, 'chapter/', $chapnum1)" />  <!-- chapter's uri-->
				<xsl:variable name="chapartnum" select= "substring-after(@eId,'art/')" />  <!--1/, article number-->
				<xsl:variable name="chaparturi" select= "concat($chapuri1, 'article/', $chapartnum)" />  <!-- article's uri-->
				<eli:LegalResource rdf:about="{$chaparturi}">
				<eli:LegalResourceSubdivision rdf:resource="http://legislation.ntua.gr/eli#Article"/>
				<eli:number><xsl:value-of select="num" /></eli:number>
				<xsl:for-each select="paragraph"> <!--uri for each paragraph-->
					<xsl:variable name="parnum" select= "substring-after(@eId,'para/')" /> <!--1/, paragraph number-->
					<eli:has_part rdf:resource="{concat($chaparturi, 'paragraph/', $parnum)}"/>	<!-- paragraph's uri-->
				</xsl:for-each>
				</eli:LegalResource>
			</xsl:for-each>
				
			<xsl:for-each select="chapter/article"> <!--text of each article-->
				<xsl:if test="./content/p"> <!--if we have no content for the article itself, we don't need to make a new node for this-->
					<xsl:variable name="chapnum2" select= "substring-after(../@eId,'/')" />  <!--1/, chapter number-->
					<xsl:variable name="chapuri2" select= "concat($rooturi, 'chapter/', $chapnum2)" />  <!-- chapter's uri-->
					<xsl:variable name="chapartnum1" select= "substring-after(@eId,'art/')" />  <!--1/, article number-->
					<xsl:variable name="chaparturi1" select= "concat($chapuri2, 'article/', $chapartnum1)" />  <!-- article's uri-->
					<eli:LegalResource rdf:about="{$chaparturi1}">
					<eli:LegalResourceSubdivision rdf:resource="http://legislation.ntua.gr/eli#Article"/>
					<text xmlns="http://legislation.ntua.gr/eli#" xml:lang="{$language}"> <xsl:value-of select="content/p"/></text>
					</eli:LegalResource>
				</xsl:if>
			</xsl:for-each>
			
			<xsl:for-each select="chapter/article/paragraph"> <!--text of each paragraph-->
				<xsl:variable name="chapnum3" select= "substring-after(../../@eId,'/')" />  <!--1/, chapter number-->
				<xsl:variable name="chapuri3" select= "concat($rooturi, 'chapter/', $chapnum3)" /> <!-- chapter's uri-->
				<xsl:variable name="chapartnum2" select= "substring-after(../@eId,'art/')" />  <!--1/, article number-->
				<xsl:variable name="chaparturi2" select= "concat($chapuri3, 'article/', $chapartnum2)" /> <!-- article's uri-->
				<xsl:variable name="chapartparnum" select= "substring-after(@eId,'para/')" />  <!--1/, paragraph number-->
				<xsl:variable name="chapartparuri" select= "concat($chaparturi2, 'paragraph/', $chapartparnum)" />  <!-- paragraph's uri-->
				<eli:LegalResource rdf:about="{$chapartparuri}">
				<eli:LegalResourceSubdivision rdf:resource="http://legislation.ntua.gr/eli#Paragraph"/>
				<eli:number><xsl:value-of select="num" /></eli:number>
				<text xmlns="http://legislation.ntua.gr/eli#" xml:lang="{$language}"> <xsl:value-of select="content/p"/></text>
				</eli:LegalResource>
			</xsl:for-each>
	</xsl:template>

</xsl:stylesheet>